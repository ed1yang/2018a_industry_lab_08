package ictgradschool.industry.lab08.ex04;


import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {

        // TODO Implement this with a Scanner
        File file=new File(fileName);
        int count=0;

        try (Scanner scanner=new Scanner(new FileReader(file))) {
            while(scanner.hasNextLine()) {
                scanner.nextLine();
                count++;
            }
        }catch(IOException e){
            System.out.println("IOException");
        }
        Movie[] films=new Movie[count];
        try (Scanner scanner=new Scanner(new FileReader(file))) {
            scanner.useDelimiter(",|\\r\\n");
            for(int i=0;scanner.hasNext();i++) {
                String name = scanner.next();
                int year= scanner.nextInt();
                int time=scanner.nextInt();
                String director=scanner.next();
                films[i]=new Movie(name,year,time,director);
            }
        }catch(IOException e) {
            System.out.println("IOException");
        }


        return films;
    }

    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}
