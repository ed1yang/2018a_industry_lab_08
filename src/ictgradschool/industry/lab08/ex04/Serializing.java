package ictgradschool.industry.lab08.ex04;

import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieWriter;
import java.io.*;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Serializing extends MovieWriter {

    @Override
    protected void saveMovies(String fileName, Movie[] films) {

        // TODO Implement this with a PrintWriter
        File file= new File(fileName);
        try(FileOutputStream fops=new FileOutputStream(file);
        ObjectOutputStream oos= new ObjectOutputStream(fops)){
            oos.writeInt(films.length);
            for(int i=0;i<films.length;i++){
                oos.writeUTF(films[i].getName());
                oos.writeInt(films[i].getYear());
                oos.writeInt(films[i].getLengthInMinutes());
                oos.writeUTF(films[i].getDirector());
            }

        }catch(IOException e){
            e.getStackTrace();
        }


    }

    public static void main(String[] args) {
        new Serializing().start();
    }

}
