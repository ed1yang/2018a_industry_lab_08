package ictgradschool.industry.lab08.ex04;

import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieReader;
import java.io.*;
/**
 * Created by anhyd on 20/03/2017.
 */
public class Deserializing extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {
        File file=new File(fileName);
        try(FileInputStream fos =new FileInputStream(file);
        ObjectInputStream ois= new ObjectInputStream(fos)){
            int length = ois.readInt();
            Movie[] films = new Movie[length];
            for (int i = 0; i < length; i++) {
                String name = ois.readUTF();
                int year = ois.readInt();
                int minute = ois.readInt();
                String director = ois.readUTF();
                films[i] = new Movie(name, year, minute, director);
                System.out.println(films[i]);
            }
            System.out.println("Movies loaded successfully from " + fileName + "!");
            return films;

        }catch(IOException e){
            e.getStackTrace();
            return null;
        }

        // TODO Implement this with a Scanner


    }

    public static void main(String[] args) {
        new Deserializing().start();
    }
}
