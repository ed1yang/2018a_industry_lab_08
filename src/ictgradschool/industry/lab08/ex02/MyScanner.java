package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;

import java.io.*;
import java.util.*;

public class MyScanner {

    public void start() {

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a Scanner.
        System.out.println("Enter file name:");
        String name= Keyboard.readInput();

        File file=new File(name);

        try(Scanner scanner = new Scanner(file)){
            while (scanner.hasNextLine()) {

                System.out.println(scanner.nextLine());
            }

        }catch(FileNotFoundException e){
            System.out.println("File not found.");
        }


    }

    public static void main(String[] args) {
        new MyScanner().start();
    }
}
