package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;

import java.io.*;

public class MyReader {

    public void start() {

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a BufferedReader.
        System.out.println("Enter file name");
        String name= Keyboard.readInput();
        File file = new File(name);

        try (BufferedReader bR = new BufferedReader(new FileReader(file))) {
            String s;
            while (((s = bR.readLine()) != null)) {
                    System.out.println(s);
                }
            } catch (FileNotFoundException e1) {
            System.out.println("File not found");
        } catch (IOException e1) {
            System.out.println("I/O Error");
        }
    }



    public static void main(String[] args) {
        new MyReader().start();
    }
}
