package ictgradschool.industry.lab08.ex01;

import java.io.*;

public class ExerciseOne {

    public void start() {

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.
            FileReader fR = null;
            int ascii=0;
            try {
                fR = new FileReader("input2.txt");
                do{
                    total++;
                    ascii=fR.read();
                    if (ascii==69|ascii==101){
                        numE++;
                    }
                }while(ascii!=-1);
                fR.close();
            } catch(IOException e) {
                System.out.println("IO problem");
            }

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.
        int c;
        BufferedReader bR;
        try {
             bR = new BufferedReader(new FileReader("input2.txt"));
            do{
                total++;
                c=bR.read();
                if (c==69|c==101){
                    numE++;
                }
            }while(c!=-1);
            bR.close();
        } catch(IOException e) {
            System.out.println("IO problem");
        }


        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}
